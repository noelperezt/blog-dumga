<?php
namespace Blog_dumga_Project;

use fw_Klipso\kernel\engine\middleware;


$route = new  middleware\Urls();

/*
    Write each of the url, example
    $route->add('','website.WebsiteController.home');

    Website is the application name. WebsiteController is the filename php and the class name and home
    is the method that is invoked when the url is called from the browser

    In this example, the home of website is in the WebsiteController

    para que alguna parte de la url sea enviada como parametro al controlador debe ser con el siguiente formato de
        ejemplo P{([0-9]+)}
*/
require BASE_DIR . '/apps/Admin/urls.php';
require BASE_DIR . '/apps/Blog/urls.php';

$route->submit();