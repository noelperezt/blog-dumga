$('#cambiar_archivo').click(function(){
    NProgress.start();
    $('#archivo').trigger('click');


});

$('#archivo').change(function(e){
    canvasImagen(e);
});
function detalleArchivo(file){
    if (file.size > 1024 * 1024)
        fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
    else
        fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

    $('#nombre_imagen').val(file.name);
    $('#size_image').text(fileSize);
    console.log(fileSize);
    $('#type_size').text(file.type);
}
function canvasImagen(evt){
    var files = evt.target.files[0];
    var reader = new FileReader();
    var tipo_archivo = files.type;



    reader.onload = (function (theFile) {
        return function (e) {
            var image = new Image();
            image.src = e.target.result;
            image.onload = function () {
                if (image.width < 400) {
                    mostrarMensaje("Por favor ingrese una imagen mas grande");
                    return false;
                } else if (image.height < 300) {
                    mostrarMensaje("Por favor ingrese una imagen mas grande");
                    return false;
                }
                detalleArchivo(files);
                //console.log(image.width);
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext("2d");
                ctx.drawImage(image, 0, 0);
                var MAX_WIDTH = 800;
                var MAX_HEIGHT = 800;
                var width = image.width;
                var height = image.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(this, 0, 0, width, height);

                var dataurl = canvas.toDataURL("image/png");

                $('#imagen_muestra').attr('src',dataurl);


            };
        };
    })(files);
    // Read in the image file as a data URL.
    reader.readAsDataURL(files);
}