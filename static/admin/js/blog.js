$(document).ready(function(){
    $('#form_filtros select').change(function(){
        $('#form_filtros').submit();
    })
    if (typeof NProgress != 'undefined') {
        $(document).ready(function () {
            NProgress.start();
        });
        $(window).load(function () {
            NProgress.done();
        });
    }


});

function mostrarMensaje(msj){
    $.notify({
        title : '<h4>Mensaje del sistema</h4>',
        icon: 'pe-7s-paper-plane',
        message: msj


    },{
        type: 'info',
        timer: 4000
    });
}