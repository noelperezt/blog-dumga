<?php

namespace apps\Admin\models;

use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\Constrainst;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

class Banners extends aModels
{
    private $prefix_model = 'blog';

    public function __fields__()
    {
        $field = [
            'banner_id' => DataType::FieldAutoField(),
            'archivo_id' => DataType::FieldInteger(true),
            'estado' => DataType::FieldChar(true,'A')
            
        ];
        return $field;
    }

    public function __setPrimary(){
        $fk = [
            'banner_id'
        ];
        return $fk;
    }

    public function __setUnique(){ 
        $uniq = [
            'archivo_id'
        ];
        return $uniq;
    }

    public function __foreignKey()
    {
        $fk = [
            'archivo_id' => Constrainst::ForeignKey('Multimedia', 'multimedia_id', Constrainst::on_delete(false)),

        ];
        return $fk;


    }
    public function __getPrefix()
    {
        return $this->prefix_model;
    }
}