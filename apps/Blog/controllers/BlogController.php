<?php
namespace apps\Blog\controllers;

use fw_Klipso\kernel\classes\abstracts\aController;
use fw_Klipso\kernel\engine\middleware\Request;

class BlogController extends aController{

    /* Write the code of the methods that are associated with the url.
    Important the method must be public */
    public function __construct($app)
    {
        parent::__construct($app);
    }

    public function index(Request $request){
        $this->render('home');
    }
}