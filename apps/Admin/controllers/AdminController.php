<?php
namespace apps\Admin\controllers;

use fw_Klipso\applications\images\UploadFile;
use fw_Klipso\applications\login\Login;
use fw_Klipso\kernel\classes\abstracts\aController;
use fw_Klipso\kernel\engine\middleware\Request;

class AdminController extends aController{

    /* Write the code of the methods that are associated with the url.
    Important the method must be public */
    public function __construct($app)
    {
        parent::__construct($app);
    }

    public function login(Request $request){
        if($request->isPost()){
            $login = new Login();

            if($login->makeLogin($request->_post('usuario'), $request->_post('pass'))){
                redirect('/admin/dashboard/');
            }
        }

        $this->render('login');
    }
    public function dashboard(Request $request){
        $context = [
            'titulo' => 'Dashboard'
        ];
        $this->render('dashboard', $context);
    }

    public function cerrar_sesion(Request $request){
        $login = new Login();
        $login->makeLogout();
    }
    public function usuario(Request $request){
        if($request->isPost()){
            die('mandaste el form');
        }
        $context = [
            'titulo' => 'Registrar usuario'
        ];
        $this->render('usuario', $context);
    }
    public function usuario_listado(Request $request){
        $context = [
            'titulo' => 'Listado de usuarios'
        ];
        $this->render('usuario_listado', $context);
    }

    public function post_listado(Request $request){
        $context = [
            'titulo' => 'Listado de post',

        ];

        if(count($request->_getAll()))
            $context = array_merge($context, $request->_getAll());

        $this->render('post_listado', $context);
    }

    public function post_crear(Request $request){
        $context = [
            'titulo' => 'Publicar post',
            'peso_maximo' => ini_get('upload_max_filesize')
        ];
        $this->render('post', $context);
    }

    public function subir_archivos(Request $request){
        $img = new UploadFile();
        $archivo = $img->setUploadImage($request->_files('archivo'));
        response_json(['link' => $archivo]);
    }
    public function categorias_crear(Request $request){
        $context = [
            'titulo' => 'Craer categorias',

        ];
        $this->render('categorias', $context);

    }

    public function multimedia_listado(Request $request){
        $context = [
            'titulo' => 'Listado de archivos multimedia',

        ];
        $this->render('multimedia_listado', $context);

    }
}