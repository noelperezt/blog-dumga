<?php

namespace apps\Admin\models;

use fw_Klipso\applications\login\models\User;
use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

class Usuario extends User
{
    private $prefix_model = 'blog';

    public function __fields__()
    {
        /*
        * Create a variable that stores an array with the fields that your model will have. Then returns that variable
        *
        * $field = [
        *    'campo1' => DataType::FieldString(200, true),
        *    'campo2' => DataType::FieldString(200, true),
        * ];
        *
        * return $field;
        *
        */
        $field = [
            'avatar' =>DataType::FieldString(100, false),
        ];
        return array_merge(parent::__fields__(), $field);
    }

    public function __setPrimary()
    {
        /* Create the primary key of your model by creating a variable that stores the field that will be PK. for example.
         * Then returns that variable
         *
         * $pk = [
         *     'campo1'
         * ];
         *
         * return $pk;
         *
         */
        return parent::__setPrimary();
    }

    public function __setUnique()
    {
        /* Create unique fields for your model by creating a variable that stores those cmpos. for example.
         * Then returns that variable
         *
         * $uniq = [
         *     'campo1'
         * ];
         *
         * return $uniq;
         *
         */
        return parent::__setUnique();
    }

    public function __foreignKey()
    {
        /* It creates foreign keys, storing in an array variable each field that has relation to foreign models in
         * the following way.
         *
         * $fk = [
         *     'campo1' => Constrainst::ForeignKey('Name_of_foreign_model','Relational_field_of_the_foreign_model',Constrainst::on_delete(false)),
         *     'campo2' => Constrainst::ForeignKey('Name_of_foreign_model','Relational_field_of_the_foreign_model')
         * ];
         * return $fk;
         *
         */
        return parent::__foreignKey();

    }
    public function __getPrefix()
    {
        return $this->prefix_model;
    }
}