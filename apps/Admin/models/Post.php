<?php

namespace apps\Admin\models;

use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\Constrainst;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

class Post extends aModels
{
    private $prefix_model = 'blog';

    public function __fields__()
    {
        $field = [
            'post_id' => DataType::FieldAutoField(),
            'titulo' => DataType::FieldString(100,true),
            'url' => DataType::FieldString(200,true),
            'descripcion' => DataType::FieldText(true),
            'cantidad_vistas' => DataType::FieldInteger(true,0),
            'imagen_id' => DataType::FieldInteger(true),
            'autor_id' => DataType::FieldInteger(true),
            'categoria_id' => DataType::FieldInteger(true),
            'fecha_creacion' =>DataType::FieldDateTime(true, DefaultDateTimeNow()),
            'estado' => DataType::FieldChar(true,'P')
        ];
        return $field;
    }

    public function __setPrimary()
    {
        $pk = [
            'post_id'
        ];
        return $pk;
    }

    public function __setUnique()
    {
        $uniq = [
            'titulo','url'
        ];
        return $uniq;
    }

    public function __foreignKey()
    {
        $fk = [
            'imagen_id' => Constrainst::ForeignKey('Multimedia', 'multimedia_id', Constrainst::on_delete(false)),
            'autor_id' => Constrainst::ForeignKey('Usuario', 'user_id', Constrainst::on_delete(false)),
            'categoria_id' => Constrainst::ForeignKey('Categoria', 'categoria_id', Constrainst::on_delete(false)),
        ];
        return $fk;

    }
    public function __getPrefix()
    {
        return $this->prefix_model;
    }
}